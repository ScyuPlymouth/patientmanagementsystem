/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientsystem;
import java.util.*;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Platform.exit;

/**
 *
 * @author user
 */
public class Patient extends User{
    Scanner input = new Scanner(System.in);
    CommandLine_PatientSystem ps = new CommandLine_PatientSystem();
    Date date = new Date();
    SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
    
    String day, month, year, name, imin;
    int hours, min; 
    private boolean rs;
    
    @Override
    public boolean func(String uniqueId, String fname, String lname){
        System.out.println("========================================");
        System.out.println("[function]");
        System.out.println("0 : Log out");
        System.out.println("1 : Delet account");
        System.out.println("2 : Rating a doctor");
        System.out.println("3 : View doctor's rating");
        System.out.println("4 : Appointment request");
        System.out.println("========================================");
        System.out.print("Which function would you like yo use: ");
        int ans = input.nextInt();
        if(ans == 0){
            exit();
            return false;
        } else if (ans == 1){
            terminationRequest(uniqueId, fname, lname);
            return true;
        } else if (ans == 2){
            rating();
            return true;
        } else if (ans == 3){
            try {
                viewRating();
            } catch (IOException ex) {
                Logger.getLogger(Patient.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        } else if (ans == 4){
            appointmantRequest(fname, lname);
            return true;
        }else {
            return false;
        }
    }

    public void terminationRequest(String uniqueId, String fname, String lname){
        System.out.print("Are you sure want to delect your accound ?(Y/N) ");
        String ans = input.next();
        if(ans.equals("Y")){
            String filename = "TerminationRequest/" + uniqueId +".txt";
            Serialiser serialiser = new Serialiser(filename);
            serialiser.writeObject(uniqueId);
            System.out.println("Your request has been sent successfully");
        } else if(ans.equals("N")){
            func(uniqueId, fname, lname);
        }else{}
    }
    
    public void rating(){
        System.out.println("Which doctor would you like to rate (Please input full name):");
        File folder = new File("DoctorName/");
        File[] listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.println("Docter :" + listOfFiles[i].getName());
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory " + listOfFiles[i].getName());
          }
        }
        String ans = input.next();
        File check = new File("DoctorName/");
        File[] listOfFiles2 = check.listFiles();
        boolean haveFile = false;
        if(haveFile == false){
            for (int i = 0; i < listOfFiles2.length; i++) {
                if (listOfFiles2[i].isFile() ) {
                    if(ans.equals(listOfFiles2[i].getName())){
                        haveFile = true;
                        break;
                    }
                }
            }
        }
        if(haveFile == false){
            System.out.println("Don't have this doctor.");
            return;
        }
        
        System.out.println("How many score will you give that doctor ?(1-5)");
        int score = input.nextInt();
        if(score > 5 || score < 0){
            while(rs == false){
                System.out.println("The score is out of range.");
                System.out.println("Please rate again.");
                score  = input.nextInt();
                if(score <= 5 && score >= 0){
                    rs = true;
                }
            }
        }
        String marks = score + "/5";
        input.nextLine();
        System.out.println("Any comments would you like say to the doctor? ");
        String comment = input.nextLine();
        
        
        String rate = "Rating/" + ans + " " + ft.format(date) + ".txt";
        Serialiser rating = new Serialiser(rate);
        rating.writeObject("Doctor :" + ans + "\r\n" + "Score :" + marks + "\r\n" + "Comment :" + comment);
    }
    
    public void viewRating() throws FileNotFoundException, IOException{
        System.out.println("Which rating do you want to read (Please input full txt's name):");
        File folder = new File("Rating/");
        File[] listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.println(listOfFiles[i].getName());
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory " + listOfFiles[i].getName());
          }
        }
        System.out.println("========================================");
        input.nextLine();
        String ans = input.nextLine();
        File file = new File("Rating/" + ans); 
        BufferedReader br = new BufferedReader(new FileReader(file));
        
        int line = 3;
        for(int i = 0 ; i<line ; i++){
            System.out.println(br.readLine());
        }
        br.close();
    }
    
     public static boolean isValidDateFormat(String format, String value) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return date != null;
    }
    
    public void appointmantRequest(String fname, String lname){
        System.out.println("Welcome to request system.");
        System.out.println("When you are free to have appointment");
        System.out.print("day (DD):");
        day = input.next();
        System.out.print("mouth (MM):");
        month = input.next();
        if(isValidDateFormat("dd/MM/yyyy" , day + "/" + month + "/2019") == true){
            System.out.println("What time are you free to have appointment ? (9:00-17:00)");
            input.nextLine();
            String time = input.nextLine();
            String[] parts = time.split(":");
            String part1 = parts[0];
            String part2 = parts[1];
            hours = Integer.parseInt(part1);
            min = Integer.parseInt(part2);
            
 
            if ((hours < 9 || hours > 17) || (min < 0 || min >= 60)){
                    System.out.println("Our of  our working hours");
                    return;
            }else{
                if(min>=0 && min <10){
                    imin = "0" + Integer.toString(min);
                } else {
                    imin = Integer.toString(min);
                }
                System.out.println("Do you want to ask for a specific doctor ?(Y/N)");
                String ans = input.next();
                if(!ans.equals("Y") && !ans.equals("N")){
                    System.out.println("Please input 'Y' or 'N'");
                    return;
                } else if(ans.equals("N")){
                    String requestName = "AppointmentRequest/" + fname + " " + lname + "'s appointment Request at " + hours + "." + imin + " on " + day + "-" + month + "-2019.txt";
                    Serialiser userDate = new Serialiser(requestName);
                    String requestData = "Patient name :" + fname + " " + lname + "\r\n" + 
                                            "Date :" + day + "-" + month + "-2019" + "\r\n" + 
                                            "Time :" + hours + ":" + min + "\r\n" + 
                                            "Request doctor : No";
                    userDate.writeObject(requestData);
                    System.out.println("appointment success");
                } else{
                    File folder = new File("DoctorName/");
                    File[] listOfFiles = folder.listFiles();
                    for (int i = 0; i < listOfFiles.length; i++) {
                        if (listOfFiles[i].isFile()) {
                            System.out.println("Docter :" + listOfFiles[i].getName());
                        } else if (listOfFiles[i].isDirectory()) {
                            System.out.println("Directory " + listOfFiles[i].getName());
                        }
                    }
                    System.out.println("Which dortor would you want to meet ?");
                    name = input.next();
                    File check = new File("DoctorName/");
                    File[] listOfFiles2 = check.listFiles();
                    boolean haveFile = false;
                    if(haveFile == false){
                        for (int i = 0; i < listOfFiles2.length; i++) {
                            if (listOfFiles2[i].isFile() ) {
                                if(name.equals(listOfFiles2[i].getName())){
                                    haveFile = true;
                                    break;
                                }
                            }
                        }
                    }
                    if(haveFile == false){
                        System.out.println("Don't have this doctor.");
                        return;
                    }
                    String requestName = "AppointmentRequest/" + fname + " " + lname + "'s appointment Request at " + hours + "." + imin + " on " + day + "-" + month + "-2019.txt";
                    Serialiser userDate = new Serialiser(requestName);
                    String requestData = "Patient name :" + fname + " " + lname + "\r\n" + 
                                            "Date :" + day + "-" + month + "-2019" + "\r\n" + 
                                            "Time :" + hours + ":" + min + "\r\n" + 
                                            "Request doctor :" + name;
                    userDate.writeObject(requestData);
                    
                    System.out.println("appointment success");
                }
            }
            } else {
            System.out.println("This is not a corrrect date.");
        }
        
    }
}
