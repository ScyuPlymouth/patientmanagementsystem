/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientsystem;
import java.util.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author scyu
 */
public class Administrator extends User{
    int group;
    Login log = new Login();
    CreateAccount ac = new CreateAccount();
    Scanner input = new Scanner(System.in);
    
    @Override
    public boolean func(String uniqueId, String fname, String lname){
        System.out.println("=====================================");
        System.out.println("[Function]");
        System.out.println("0 : Log out");
        System.out.println("1 : Create a new account");
        System.out.println("2 : Remove an acoound");
        System.out.println("3 : Feedback and view doctor's rating");
        System.out.println("=====================================");
        System.out.print("Which function would you like to use: ");
        int func = input.nextInt();
        if(func == 1){
            creatAccount();
            return true;
        } else if(func == 2){
            removeAccount();
            return true;
        } else if(func == 3){
            try {
                feedBack();
            } catch (IOException ex) {
                Logger.getLogger(Administrator.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        } else if(func == 0){
            return false;
        } else {
            return false;
        }
    }
    
    public void creatAccount(){
        System.out.println("what kind of accound would you like to create");
        System.out.println("1:Admin, 2:Doctor, 3:Secretary");
        group = input.nextInt();
        try{
            if(group == 1){
                ac.askData();
                ac.writeData("A", ac.idGenerator(), ac.password, ac.fname, ac.lname, ac.gender, ac.age,ac.address);
            } else if(group == 2){
                ac.askData();
                ac.writeData("D", ac.idGenerator(), ac.password, ac.fname, ac.lname, ac.gender, ac.age,ac.address);
            } else if(group == 3){
                ac.askData();
                ac.writeData("S", ac.idGenerator(), ac.password, ac.fname, ac.lname, ac.gender,ac.age, ac.address);
            } else {
                System.out.println("Don't have this group .");
            }
        }catch(InputMismatchException e){
            System.out.println("Please input a correct group.");
            creatAccount();
        }
    }
    
    public void removeAccount(){
        System.out.println("Who's account world you like to delect ?");
        System.out.print("Please input the unique id to delect an account: ");
        String ans = input.next();
        File file = new File(ans + ".txt"); 
        if(file.delete()) 
        { 
            System.out.println("Account deleted successfully"); 
        } 
        else
        { 
            System.out.println("Failed to delete the Account"); 
        } 
    }

    public void feedBack() throws IOException{
        System.out.println("Which rating do you want to read (Please input full txt's name):");
            File folder = new File("Rating/");
            File[] listOfFiles = folder.listFiles();
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    System.out.println(listOfFiles[i].getName());
                } else if (listOfFiles[i].isDirectory()) {
                    System.out.println("Directory " + listOfFiles[i].getName());
              }
            }
            System.out.println("========================================");
            input.nextLine();
            String ans = input.nextLine();
            File file = new File("Rating/" + ans); 
            BufferedReader br = new BufferedReader(new FileReader(file));

            int line = 3;
            for(int i = 0 ; i<line ; i++){
                System.out.println(br.readLine());
            }
            br.close();
            System.out.println("Would you like to have a feedback on it?(Y/N)");
            String fb = input.next();
            if(fb.equals("Y")){
                String[] parts = ans.trim().split("\\.");
                String part1 = parts[0];
                String part2 = parts[1];
                
                File aaa = new File("AdminFeedbacks/" + part1 + " feedback"); 
                String feedback = aaa + ".txt";
                Serialiser feedbackfile = new Serialiser(feedback);
                file = new File("Rating/" + ans);
                BufferedReader br2 = new BufferedReader(new FileReader(file));
                line = 3;
                String lines = "";
                for(int i = 0 ; i<line ; i++){
                    lines += "\r\n" + br2.readLine();
                }
                br2.close();
                System.out.println("Feedback :");
                input.nextLine();
                String fbl = input.nextLine();
                feedback = "Case:" + part1 + lines + "\r\n" + "==========================" + "\r\n" + "Feedback :" + fbl;
                feedbackfile.writeObject(feedback);
                
            }else{
                br.close();
            }
    }
}
