/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientsystem;
import java.util.*;
import java.io.*;
import static patientsystem.Patient.isValidDateFormat;
/**
 *
 * @author scyu
 */
public class CreateAccount extends User implements Serializable{
    Scanner input = new Scanner(System.in);
    Login log = new Login();
    
    public String password, fname, lname, address, st, birthday;
    char gender;
    int age;
    
    public void approval() throws IOException{
        String group = "P";
        System.out.println("Did you get secretary's approval ? (Y/N)");
        String ans = input.next();
        if(ans.equals("Y")){
            try{
                System.out.print("Please input your approve code from secretary: ");
                String code = input.next();
                File file = new File("src/ApprovalCode/" + code + ".txt");
                BufferedReader br = new BufferedReader(new FileReader(file));
                int line = 1;
                for(int i = 0 ; i<line ; i++){
                    st = br.readLine();
                }
                br.close();
                System.out.println(code);
                if(st.equals(code)){
                    System.out.println(file);
                    if(file.delete()){
                            askData();
                            writeData(group, idGenerator(), password, fname, lname, gender, age,address);
                        }
                    } else{
                        System.out.println("That isn't an approve code."); 
                }
            }catch(FileNotFoundException e){
                System.out.println("That isn't an approve code.");
            }
                    
        } else if (ans.equals("N")){
            System.out.println("Please ask scretary for the approval first.");
            System.out.println("Press Enter to exit the system.");
            String exit = input.next();
            if (exit.equals("")){
                    System.exit(0);
               } else {
                    while(!exit.equals("")){
                        System.out.println("Press Enter to exit the system.");
                        exit = input.next();
                    }
            }
        }else{
            System.out.println("Please ask scretary for the approval first.");
        }
    }
    
    public void askData(){
        uniqueId = idGenerator();
        System.out.println("Welcome to Rigister Page !");
        System.out.println("System will auto generate you unique id, please mark down the id for next login after register!");
        System.out.println("Please set your password first");
        password = input.next();
        System.out.println("First name :");
        fname = input.next();
        System.out.println("Last name :");
        lname = input.next();
        
        System.out.println("Gender :");
        gender = input.next().charAt(0);
        while(gender != 'M' && gender != 'F'){
            System.out.println("Please input 'M' or 'F' !");
            gender = input.next().charAt(0);
        }
        
        System.out.println("Birthday : (DD/MM/YYYY)");
        birthday = input.next();

        if(isValidDateFormat("dd/MM/yyyy" , birthday) == true){
            String[] parts = birthday.split("/");
            String part1 = parts[0];
            String part2 = parts[1];
            String part3 = parts[2];
            int year = Integer.parseInt(part3);
            age = 2019 - year;
        } else {
            System.out.println("the birthday format is not correct");
        }
            
            
        System.out.println("Address :");
        address = input.next();
    }
    
    public String idGenerator(){
        Random rand = new Random();
        String uid = "";
        for (int i = 0 ; i < 4 ; i++ ){
            int id = rand.nextInt(9) + 1;
            uid += Integer.toString(id);
        }
        return uid;
    }
    
    
    public String data(String uniqueId, String password, String fname, String lname, char gender, int age,String address){
        String data =   "uniqueId :" + uniqueId + "\r\n" +
                        "Password :" + password + "\r\n" +
                        "fname :" + fname + "\r\n" +
                        "lname :" + lname + "\r\n" + 
                        "Gender :" + gender + "\r\n" +
                        "age :" + age + "\r\n" +
                        "Address :" + address + "\r\n";
        return data;
    }

    public void writeData(String group, String uniqueId, String password,String fname, String lname, char gender, int age,String address){ 
        String filename = "UserData/" + group + uniqueId +".txt";
        String name = "DoctorName/" + fname + "_" + lname;
        Serialiser userDate = new Serialiser(filename);
        Serialiser doctorName = new Serialiser(name);
        if(group.equals("P")){
            userDate.writeObject(data("P" +uniqueId, password, fname, lname, gender, age,address));
            System.out.println("Your uniqued id is :P" + uniqueId);
        }else if(group.equals("A")){
            userDate.writeObject(data("A" +uniqueId, password, fname, lname, gender, age,address));
            System.out.println("Your uniqued id is :A" + uniqueId);
        }else if(group.equals("D")){
            userDate.writeObject(data("D" +uniqueId, password, fname, lname, gender, age,address));
            System.out.println("Your uniqued id is :D" + uniqueId);
            doctorName.writeObject("");
        }else if(group.equals("S")){
            userDate.writeObject(data("S" +uniqueId, password, fname, lname, gender, age,address));
            System.out.println("Your uniqued id is :D" + uniqueId);
        }
    }
}
