/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientsystem;

/**
 *
 * @author user
 */
public class CheckGroup {
    Administrator admin = new Administrator();
    Secretary scretary = new Secretary();
    Patient patient = new Patient();
    public void checking(char group, String uniqueId, String fname, String lname){
        if(group =='P'){
            System.out.println("Welcome to PatientSystem !");
            assignGroup(group, uniqueId, fname, lname);
        }else if(group =='A'){
            System.out.println("Welcome to Admin Page !");
            assignGroup(group, uniqueId, fname, lname);
        }else if(group =='D'){
            System.out.println("Welcome to Doctor Page !");
            assignGroup(group, uniqueId, fname, lname);
        }else if(group =='S'){
            System.out.println("Welcome to Secretary Page !");
            assignGroup(group, uniqueId, fname, lname);
        }
    }
    
    public void assignGroup(char group, String uniqueId, String fname, String lname){
        if(group == 'A'){
            while(admin.func(uniqueId, fname, lname)==true){
                admin.func(uniqueId, fname, lname);
            }
        } else if(group == 'S'){
            while(scretary.func(uniqueId, fname, lname)==true){
                scretary.func(uniqueId, fname, lname);
            }
        } else if(group == 'P'){
            while(patient.func(uniqueId, fname, lname)==true){
                patient.func(uniqueId, fname, lname);
            }
        } 
    }
}
