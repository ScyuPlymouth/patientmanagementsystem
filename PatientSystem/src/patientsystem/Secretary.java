/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientsystem;
import java.util.*;
import java.io.*;
import java.nio.channels.FileChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author scyu
 */
public class Secretary extends User{
    Scanner input = new Scanner(System.in);
    char group;
    public String code;
    
    
    @Override
    public boolean func(String uniqueId, String fname, String lname){
        System.out.println("=====================================");
        System.out.println("[Function]");
        System.out.println("0 : Log out");
        System.out.println("1 : Generate an approvement code");
        System.out.println("2 : Remove a patient account");
        System.out.println("3 : Accept termination request");
        System.out.println("4 : View appointment request from patient");
        System.out.println("5 : Create an appointment between a patient and a free doctor");
        System.out.println("=====================================");
        System.out.print("Which function would you like to use: ");
        int func = input.nextInt();
        if(func == 1){
            approveGenerator();
            return true;
        } else if(func == 2){
            removePatient();
            return true;
        } else if(func == 3){
            acceptTerminateRequest();
            return true;
        } else if(func == 4){
            try {
                viewRequest();
            } catch (IOException ex) {
                Logger.getLogger(Secretary.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        } else if(func == 5){
            createAppoint();
            return true;
        } else if(func == 0){
            return false;
        } else {
            return false;
        }
    }
    
    public String data(String code){
        String data = code;
        return data;
    }
    
    public String approveGenerator(){
        Random rand = new Random();
            code = "";
            for (int i = 0 ; i < 4 ; i++ ){
                int id = rand.nextInt(9) + 1;
                code += Integer.toString(id);
            }
            code = code + "-" ;
            for (int i = 0 ; i < 4 ; i++ ){
                int id = rand.nextInt(9) + 1;
                code += Integer.toString(id);
            }
            code = code + "-" ;
            for (int i = 0 ; i < 4 ; i++ ){
                int id = rand.nextInt(9) + 1;
                code += Integer.toString(id);
            }
            code = code + "-" ;
            for (int i = 0 ; i < 4 ; i++ ){
                int id = rand.nextInt(9) + 1;
                code += Integer.toString(id);
            }
            System.out.println("Code generated");
            System.out.println("Code :" + code);
            
            String filename = "src/ApprovalCode/" + code + ".txt";
            Serialiser serialiser = new Serialiser(filename);
            serialiser.writeObject(data(code));

        return code;
    }
    
    public void removePatient(){
        System.out.println("Please input the unique id to remove that accound");
        String uniqueId = input.next();
        File file = new File("Userdata/" + uniqueId + ".txt"); 
        stringToChar(uniqueId);
        if(group!='P'){
            System.out.println("You can't remove not patient's account");
        }else{
            if(file.delete()) 
            { 
                System.out.println("Account deleted successfully"); 
            } else { 
                    System.out.println("Failed to delete the Account"); 
            } 
        }
    }
    
    public void acceptTerminateRequest(){
        System.out.print("Please input the unique id who are going to delect account :");
        String id = input.next();
        File file = new File("TerminationRequest/" + id + ".txt"); 
        File file2 = new File("UserData/"+ id + ".txt");
        if(file.delete()) 
        { 
            file2.delete();
            System.out.println("Account deleted successfully !"); 
        } 
        else
        { 
            System.out.println("That patient is not going to delect the account."); 
        } 
    }
    
    public void stringToChar(String string){
        char[] allChars = new char[string.length()];
            for (int i = 0; i < string.length(); i++) {
                allChars[i] = string.charAt(i);
            }
        group = allChars[0];
        }
    
    public void viewRequest() throws FileNotFoundException, IOException{
        File folder = new File("AppointmentRequest/");
        File[] listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.println(listOfFiles[i].getName());
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory " + listOfFiles[i].getName());
          }
        }
        
        System.out.print("Which appointment request would you like to read :");
        input.nextLine();
        String name = input.nextLine();
                
        File file = new File("AppointmentRequest/" + name); 
        BufferedReader br = new BufferedReader(new FileReader(file)); 
        String line = null;
        System.out.println("=====================================");
        System.out.println("[Appointment Request details]");
        while((line = br.readLine()) !=null){
            System.out.println(line);
        }
        System.out.print("Do you accept this request ?(Y/N)");
        String ans = input.next();
        if(ans.equals("N")){
            File request = new File("AppointmentRequest/" + name); 
            if(request.delete()) 
            { 
                System.out.println("Account deleted successfully"); 
                } else
                { 
                    System.out.println("Failed to delete the Account"); 
                    } 
        } else if (ans.equals("Y")){
                System.out.println("The appointment will send to :");
                File dest = new File("DoctorAppointment/" + name);
                File dest2 = new File("PatientAppointment/" + name);
                copyFileUsingChannel(file, dest);
                copyFileUsingChannel(file, dest2);
        }
    }
    
    public void createAppoint(){
    
    }
    
    private static void copyFileUsingChannel(File source, File dest) throws IOException {
    FileChannel sourceChannel = null;
    FileChannel destChannel = null;
    try {
        sourceChannel = new FileInputStream(source).getChannel();
        destChannel = new FileOutputStream(dest).getChannel();
        destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        }finally{
           sourceChannel.close();
           destChannel.close();
       }
    }
    
}
