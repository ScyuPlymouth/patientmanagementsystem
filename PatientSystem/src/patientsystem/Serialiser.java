package patientsystem;

/**
 *
 * @author scyu
 */
import java.io.*;

public class Serialiser {
    
    private String name;
    
    Serialiser(String filename){
        name = filename;
    }
    
    public void setName(String filename){
        name = filename;
    }
    
    public String getName(){
        return name;
    }
    
    public Serializable readObject(){
        Serializable loadedObject = null;
        try {
         FileInputStream fileIn = new FileInputStream(name);
         ObjectInputStream in = new ObjectInputStream(fileIn);
         loadedObject = (Serializable) in.readObject();
         in.close();
         fileIn.close();
         System.out.println("Data loaded from: "+ name);
        } catch (IOException i) {
            System.out.println("File not found.");
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("Class not found");
            c.printStackTrace();
        }
        return loadedObject;
    }
    
    /**
     *
     * @param object
     * @return
     */
    public boolean writeObject(Serializable object){
        try {
            PrintStream file =  new PrintStream(new FileOutputStream(name));
            FileOutputStream fileOut = new FileOutputStream(name);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            file.println(object);
            //out.writeObject(object);
            
            out.close();
            fileOut.close();
            return true;
         } catch (IOException i) {
            System.out.println("Failed to load!");
            i.printStackTrace();
            return false;
         }
    }
    
}
